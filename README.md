# README #

Sennheiser EM6000 Wireless Microphone Module by Wayne Osborn

### Overview ###
The purpose of this module is to provide monitoring of the dual-channel Radio Mic receiver unit.

The code polls at 5 second intervals for the Receiver Name, Frequency and Battery levels.
 
More timely updates of the RF and LQI meters are handled by 1 second "mm" subscriptions. 

RF level meters are provided with dBm scaled values using the Sennheiser supplied formula.

### Network ###
The module communicates with UDP on port 6970.

### Test Demo ###
A Test Demo mode is built in (per channel) that can simulate hardware response data when EM6000 hardware is not present or 
when Emulating.

### Contact ###
Wayne Osborn
+61432683728
wayne@osbornaudio.com.au